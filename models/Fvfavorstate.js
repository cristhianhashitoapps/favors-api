"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvfavorstate = sequelize.define('Fvfavorstate', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		name: { type: DataTypes.STRING, allowNull: false},
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true}
	}, {
		classMethods: {
			associate: function (models) {
				Fvfavorstate.hasOne(models.Fvfavor);
			}
		}
	});

	return Fvfavorstate;
};
