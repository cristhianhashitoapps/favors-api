"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvtransactionstate = sequelize.define('Fvtransactionstate', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		name: { type: DataTypes.STRING, allowNull: false},
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true}
	}, {
		classMethods: {
			associate: function (models) {
				Fvtransactionstate.hasOne(models.Fvtransaction);
			}
		}
	});

	return Fvtransactionstate;
};
