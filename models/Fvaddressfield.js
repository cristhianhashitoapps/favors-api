"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvaddressfield = sequelize.define('Fvaddressfield', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		name: { type: DataTypes.STRING, allowNull: false},
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true}
	}, {
		classMethods: {
			associate: function (models) {
				Fvaddressfield.hasOne(models.Fvaddress);
			}
		}
	});

	return Fvaddressfield;
};
