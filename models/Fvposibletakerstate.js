"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvposibletakerstate = sequelize.define('Fvposibletakerstate', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		name: { type: DataTypes.STRING, allowNull: false},
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true}
	}, {
		classMethods: {
			associate: function (models) {
				Fvposibletakerstate.hasOne(models.Fvposibletaker);
			}
		}
	});

	return Fvposibletakerstate;
};
