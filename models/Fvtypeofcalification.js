"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvtypeofcalification = sequelize.define('Fvtypeofcalification', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		name: { type: DataTypes.STRING, allowNull: false},
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true}
	}, {
		classMethods: {
			associate: function (models) {
				Fvtypeofcalification.hasOne(models.Fvfavor);
			}
		}
	});

	return Fvtypeofcalification;
};
