"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvfavorandtype = sequelize.define('Fvfavorandtype', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true}
	}, {
		classMethods: {
			associate: function (models) {				
				Fvfavorandtype.belongsTo(models.Fvfavor);
				Fvfavorandtype.belongsTo(models.Fvfavortype);
			}
		}
	});
	return Fvfavorandtype;
};
