//en colombia departamentos
"use strict";

module.exports = function (sequelize,DataTypes) {
	var Fvcupon = sequelize.define('Fvcupon', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
        name: { type: DataTypes.STRING, allowNull: false},
        date: { type: DataTypes.DATE, allowNull: false},
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true},
		valuedolar: { type: DataTypes.INTEGER, allowNull: true, default: 0}
	});

	return Fvcupon;
};
