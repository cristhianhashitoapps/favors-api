//en colombia departamentos
"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvstate = sequelize.define('Fvstate', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		name: { type: DataTypes.STRING, allowNull: false},
		code: { type: DataTypes.STRING, allowNull: false},
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true}
	}, {
		classMethods: {
			associate: function (models) {
				Fvstate.hasMany(models.Fvcity);
				Fvstate.belongsTo(models.Fvcountry);
			}
		}
	});

	return Fvstate;
};
