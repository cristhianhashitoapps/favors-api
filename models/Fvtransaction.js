"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvtransaction = sequelize.define('Fvtransaction', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		payujsonresponse: { type: DataTypes.STRING, allowNull: true},
		payucode: { type: DataTypes.STRING, allowNull: true},
		paytransactionid: { type: DataTypes.STRING, allowNull: true},
		payustate : { type: DataTypes.STRING, allowNull: true},
		cupon : { type: DataTypes.STRING, allowNull: true},
	}, {
		classMethods: {
			associate: function (models) {
				Fvtransaction.belongsTo(models.Fvtransactionstate);
				Fvtransaction.belongsTo(models.Fvcreditcard);
				Fvtransaction.belongsTo(models.Fvfavor);
			}
		}
	});

	return Fvtransaction;
};
