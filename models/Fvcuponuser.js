//en colombia departamentos
"use strict";

module.exports = function (sequelize,DataTypes) {
	var Fvcuponuser = sequelize.define('Fvcuponuser', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
        FvuserId: { type: DataTypes.INTEGER, allowNull: false},
        cupon: { type: DataTypes.STRING, allowNull: true}		
	});

	return Fvcuponuser;
};
