"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvposibletaker = sequelize.define('Fvposibletaker', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		efective: { type: DataTypes.BOOLEAN, allowNull: false, default: true}
	}, {
		classMethods: {
			associate: function (models) {
				Fvposibletaker.belongsTo(models.Fvuser);
				Fvposibletaker.belongsTo(models.Fvfavor);
				Fvposibletaker.belongsTo(models.Fvposibletakerstate);
			}
		}
	});

	return Fvposibletaker;
};
