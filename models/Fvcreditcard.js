"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvcreditcard = sequelize.define('Fvcreditcard', {
		id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
		number: { type: DataTypes.STRING, allowNull: false },
		securitycode: { type: DataTypes.STRING, allowNull: false },
		finaldate: { type: DataTypes.DATE, allowNull: false },
		token: { type: DataTypes.STRING, allowNull: false },
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true }

	}, {
			classMethods: {
				associate: function (models) {
					Fvcreditcard.belongsTo(models.Fvuser);
					Fvcreditcard.belongsTo(models.Fvbank);
					Fvcreditcard.hasOne(models.Fvtransaction);
				}
			}
		});

	return Fvcreditcard;
};
