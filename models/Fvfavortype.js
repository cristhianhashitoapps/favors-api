"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvfavortype = sequelize.define('Fvfavortype', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		name: { type: DataTypes.STRING, allowNull: false},
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true}
	}, {
		classMethods: {
			associate: function (models) {
				Fvfavortype.hasMany(models.Fvfavorandtype);
				Fvfavortype.hasOne(models.Fvuser);
			}
		}
	});

	return Fvfavortype;
};
