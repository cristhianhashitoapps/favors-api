"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvcity = sequelize.define('Fvcity', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		name: { type: DataTypes.STRING, allowNull: false},
    code: { type: DataTypes.STRING, allowNull: false},
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true}
	}, {
		classMethods: {
			associate: function (models) {
				Fvcity.belongsTo(models.Fvstate);
			}
		}
	});

	return Fvcity;
};
