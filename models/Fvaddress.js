"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvaddress = sequelize.define('Fvaddress', {
		id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
		name: { type: DataTypes.STRING, allowNull: false },
		lat: { type: DataTypes.STRING, allowNull: true },
		lang: { type: DataTypes.STRING, allowNull: true },
		field2: { type: DataTypes.STRING, allowNull: false },
		field3: { type: DataTypes.STRING, allowNull: false },
		field4: { type: DataTypes.STRING, allowNull: false },
		complement: { type: DataTypes.STRING, allowNull: true },
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true }
	}, {
			classMethods: {
				associate: function (models) {
					Fvaddress.belongsTo(models.Fvaddressfield);
					Fvaddress.belongsTo(models.Fvuser);
					Fvaddress.belongsTo(models.Fvcity);
				}
			}
		});

	return Fvaddress;
};
