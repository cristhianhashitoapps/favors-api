"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvcountry = sequelize.define('Fvcountry', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		name: { type: DataTypes.STRING, allowNull: false},
		code: { type: DataTypes.STRING, allowNull: false},
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true}
	}, {
		classMethods: {
			associate: function (models) {
				Fvcountry.hasMany(models.Fvstate);
			}
		}
	});

	return Fvcountry;
};
