'use strict';

var Utils = {};
var sequelize = require('sequelize');
var jwt = require('jwt-simple');
var moment = require('moment');
var bcrypt = sequelize.Promise.promisifyAll(require('bcryptjs'));
var SALT_WORK_FACTOR = 10;
var MAX_LOGIN_ATTEMPTS = 5;
var LOCK_TIME = 2 * 60 * 60 * 1000;

Utils.HashPassword = function (user, options) {
  if (user._changed && user._changed.password) {
    return bcrypt
      .genSaltAsync(SALT_WORK_FACTOR)
      .then(function (salt) {
        return bcrypt
          .hashAsync(user.password, salt)
      })
      .then(function (hash) {
        user.password = hash;

        return sequelize.Promise.resolve(user);
      });
  }
};

Utils.Login = function (login, password) {
  var models = require('../models');
  var User = null;
  return models
    .Fvuser
    .find({
      where: { login: login }
    })
    .then(function (user) {
      if (!user) {
        return sequelize.Promise.reject('User does not exist');
      }
      User = user;
      return User.ComparePassword(password);
    })
    .then(function (isMatch) {
      if (!isMatch) {
        return sequelize.Promise.reject('Incorrect password');
      }
      return sequelize.Promise.resolve(Utils.CreateToken(User));
    })
    .catch(function (error) {
      return sequelize.Promise.reject(error);
    })
};


Utils.ComparePassword = function (password) {
  return bcrypt
    .compareAsync(password, this.password);
};

Utils.CreateToken = function (user) {
  var config = require('../config/config-jwt');
  var payload = {
    sub: user,
    iat: moment().unix(),
    exp: moment().add(10, 'years').unix(),
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
};

module.exports = function (sequelize, DataTypes) {
  var Fvuser = sequelize.define('Fvuser', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    login: { type: DataTypes.STRING, allowNull: false, unique: true },
    password: { type: DataTypes.STRING, allowNull: false },
    email: { type: DataTypes.STRING, allowNull: false },
    firstname: { type: DataTypes.STRING, allowNull: false },
    img: { type: DataTypes.STRING, allowNull: false },
    lastname: { type: DataTypes.STRING, allowNull: false },
    profile: { type: DataTypes.STRING, allowNull: true },
    active: { type: DataTypes.BOOLEAN, allowNull: true, default: true },
    typeofuser: { type: DataTypes.INTEGER, allowNull: false },//1 es salvadia,
    telefono: { type: DataTypes.STRING, allowNull: true },
    tokenfcm: { type: DataTypes.STRING, allowNull: true },
  }, {
      hooks: { 
        beforeCreate: Utils.HashPassword,
        beforeUpdate: Utils.HashPassword
      },
      instanceMethods: {
        ComparePassword: Utils.ComparePassword
      },
      classMethods: {
        Login: Utils.Login,
        associate: function (models) {
          Fvuser.hasMany(models.Fvcreditcard);
          Fvuser.hasOne(models.Fvfavor);
          Fvuser.hasMany(models.Fvposibletaker);
          Fvuser.belongsTo(models.Fvfavortype);
        }
      }
    });

  return Fvuser;
};
