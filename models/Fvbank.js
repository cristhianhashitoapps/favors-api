"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvbank = sequelize.define('Fvbank', {
		id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		name: { type: DataTypes.STRING, allowNull: false},
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true}
	}, {
		classMethods: {
			associate: function (models) {
				Fvbank.hasOne(models.Fvcreditcard);
			}
		}
	});

	return Fvbank;
};
