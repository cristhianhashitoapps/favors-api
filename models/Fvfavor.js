"use strict";
module.exports = function (sequelize, DataTypes) {
	var Fvfavor = sequelize.define('Fvfavor', {
		id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
		description: { type: DataTypes.STRING, allowNull: false },
		active: { type: DataTypes.BOOLEAN, allowNull: false, default: true },
		durationhours: { type: DataTypes.INTEGER, allowNull: false },
		datefavor: { type: DataTypes.DATE, allowNull: false },
		realdurationhours: { type: DataTypes.INTEGER, allowNull: false },
		price: { type: DataTypes.FLOAT, allowNull: false },
		opinion: { type: DataTypes.STRING, allowNull: false },
	}, {
			classMethods: {
				associate: function (models) {
					Fvfavor.belongsTo(models.Fvuser);
					Fvfavor.belongsTo(models.Fvfavorstate);
					Fvfavor.belongsTo(models.Fvaddress);
					Fvfavor.belongsTo(models.Fvtypeofcalification);
					Fvfavor.hasMany(models.Fvposibletaker);
					Fvfavor.hasMany(models.Fvfavorandtype);
					Fvfavor.hasOne(models.Fvtransaction);
				}
			}
		});

	return Fvfavor;
};
