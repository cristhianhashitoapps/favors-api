var models     = require('../../models');
var repository = require('../../repositories/Fvcuponuser');
var controller = require('./BaseController')(repository);

controller.GetByUserAndCupon = function (req, res) {
    var body = req.body;
    console.log(body);
    //console.log(repository)
    repository
        .GetByUserAndCupon(body.cupon,body.FvuserId)
        .then(function (models) {
            res.status(200).send(models);
        })
        .catch(function (err) {
            console.log("error "+err);
            res.status(500).send(err);
        })

}


module.exports = controller;
