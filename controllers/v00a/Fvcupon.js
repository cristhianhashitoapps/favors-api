var models     = require('../../models');
var repository = require('../../repositories/Fvcupon');
var controller = require('./BaseController')(repository);

controller.GetByName = function (req, res) {
    var name = req.params.Id;
    repository
        .GetByName(name)
        .then(function (models) {
            res.status(200).send(models);
        })
        .catch(function (err) {
            res.status(500).send(err);
        })

}


module.exports = controller;
