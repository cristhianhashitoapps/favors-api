var models = require('../../models');
var repository = require('../../repositories/Fvaddress');
var controller = require('./BaseController')(repository);

controller.byId = function (req, res) {
    var addressId = req.params.Id;
    console.log("controller", "byId", addressId);
    repository
        .byId(addressId)
        .then(function (models) {
            res.status(200).send(models);
        })
        .catch(function (err) {
            res.status(500).send(err);
        })

}

module.exports = controller;
