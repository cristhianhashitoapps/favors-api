var models     = require('../../models');
var repository = require('../../repositories/Fvcreditcard');
var repositoryTransaction = require('../../repositories/Fvtransaction');
var controller = require('./BaseController')(repository);
var request = require('request');
var creditcards = require('../../config/creditcards');
var md5 = require('md5');

controller.GetCreditCardsByUser = function(req, res) {
  console.log(creditcards.apiLogin);
  repository
    .GetCreditCardsByUser(req.params.Id)
    .then(function(model){
      res.status(200).send(model);
    })
    .catch(function(error){
      res.status(500).send(error);
    })
};

controller.SaveCreditCardInPayu= function(req,res){
  var card = req.body;
  //enviar tarjeta a payu
  var data = getDataPayuToSave(card);
  var postObject = getObjectPayu(data);
  console.log(postObject);
  request(postObject, function(error,response,body){

    var responsePayu = JSON.parse(body);
    if(responsePayu.code=="SUCCESS"){
      card.token=responsePayu.creditCardToken.creditCardTokenId;
      card.number = ["**** **** **** **** ",card.number.substring(12,16)].join("");
      repository
      .Insert(card)
      .then(function(result){
        res.status(200).send(result);
      })
      .catch(function(err){
        res.status(500).send(err);
      })
    }else{
      res.status(500).send(body);
    }
  });

}
controller.DeleteCreditCardInPayu = function(req,res){

  var card = req.body;
  var data = getDataPayuToDelete(card);
  var postObject = getObjectPayu(data);
  console.log(postObject);
  request(postObject, function(error,response,body){

    var responsePayu = JSON.parse(body);
    console.log(responsePayu);
    if(responsePayu.code=="SUCCESS"){

      repository
      .Delete(card.id)
      .then(function(result){
        res.sendStatus(200).send(result);
      })
      .catch(function(err){
        res.status(500).send(err);
      })
    }else{
      res.status(500).send(body);
    }
  });
}

controller.PayWithPayu = function(req,res){
  var transaction = req.body;
  var data = getDataPayuToPay(transaction);
  var postObject = getObjectPayu(data);
  console.log(postObject);
  request(postObject, function(error,response,body){

    var responsePayu = JSON.parse(body);
      console.log(responsePayu);
    if(responsePayu.code=="SUCCESS"){

      transaction.payucode = responsePayu.code;
      transaction.payujsonresponse = body;
      transaction.payustate = responsePayu.transactionResponse.state;
      transaction.paytransactionid = responsePayu.transactionResponse.transactionId;

      console.log(transaction);

      repositoryTransaction
      .Update(transaction.id,transaction)
        .then(function(response){
            res.status(200).send(response);
        })
        .catch(function(err){
           res.status(500).send(err);
        })

    }else{
      res.status(500).send(body);
    }
  });
}

function getDataPayuToPay(transaction) {

  var timestamp = new Date().getUTCMilliseconds();
  var referenceCode = ["pagofavores", timestamp].join("");
  var total = Number(transaction.Fvfavor.durationhours)*Number(transaction.Fvfavor.price);
  var total = transaction.valuedolar;
  var signatureJoin = [creditcards.apiKey, creditcards.merchantId, referenceCode, total, creditcards.currency].join("~");
  var signature = md5(signatureJoin);

  var res = {
    "language": creditcards.language,
    "command": "SUBMIT_TRANSACTION",
    "merchant": {
      "apiKey": creditcards.apiKey,
      "apiLogin": creditcards.apiLogin
    },
    "transaction": {
      "order": {
        "accountId": creditcards.accountId,
        "referenceCode": referenceCode,
        "description": creditcards.description,
        "language": creditcards.language,
        "signature": signature,
        "notifyUrl": "https://api.salvadia.com/api/v00a/fvtransaction/confirmpayu",
        "additionalValues": {
          "TX_VALUE": {
            "value": total,
            "currency": creditcards.currency
          }
        },
        "buyer": {
          "merchantBuyerId": transaction.Fvfavor.Fvuser.id,
          "fullName": [transaction.Fvfavor.Fvuser.username, transaction.Fvfavor.Fvuser.lastname].join(" "),
          "emailAddress": transaction.Fvfavor.Fvuser.email,
          "contactPhone": "9000",
          "dniNumber": "9000",
          "shippingAddress": {
            "street1": "calle 100",
            "street2": "5555487",
            "city": "Bogota",
            "state": "BO",
            "country": "CO",
            "postalCode": "000000",
            "phone": "(11)756312633"
          }
        }
      },
      "creditCardTokenId": transaction.Fvcreditcard.token,
      "extraParameters": {
        "INSTALLMENTS_NUMBER": 1
      },
      "type": "AUTHORIZATION_AND_CAPTURE",
      "paymentMethod": transaction.Fvcreditcard.Fvbank.name,
      "paymentCountry": "CO",
      "ipAddress": "127.0.0.1"
    },
    "test": false
  }
  return res;
}

function getDataPayuToSave(card){
  //var expirationDate =
  var cardNumber = card.number.replace(/\s/g,'');

  var res = {
    "language": "es",
    "command": "CREATE_TOKEN",
    "merchant": {
      "apiLogin": creditcards.apiLogin,
      "apiKey": creditcards.apiKey
    },
    "creditCardToken": {
      "payerId": card.FvuserId,
      "name": card.FvuserName,
      "identificationNumber": card.FvuserIdentification,
      "paymentMethod": card.FvbankName,
      "number": cardNumber,
      "expirationDate": card.finaldate
    }
  }
  return res;
}

function getDataPayuToDelete(card){
  var res = {
     "language": "es",
     "command": "REMOVE_TOKEN",
     "merchant": {
        "apiLogin": creditcards.apiLogin,
        "apiKey": creditcards.apiKey
     },
     "removeCreditCardToken": {
        "payerId": card.FvuserId,
        "creditCardTokenId": card.token
     }
  }
  return res;
}

//{"code":"SUCCESS","error":null,"creditCardToken":{"creditCardTokenId":"5be46fa0-4f3b-4133-8538-cbe3eeb8b580","name":"TEST","payerId":"10","identificationNumber":"8009001","paymentMethod":"VISA","number":null,"expirationDate":null,"creationDate":null,"maskedNumber":"459897******0625","errorDescription":null}}

function getObjectPayu(data){

  var options = {
    //uri: 'https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi',
    uri: 'https://api.payulatam.com/payments-api/4.0/service.cgi',
    body: JSON.stringify(data),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': "application/json"
    }
  }
  return options;
}

module.exports = controller;
