var models     = require('../../models');
var repository = require('../../repositories/Fvfavor');
var controller = require('./BaseController')(repository);

controller.GetFavorsByUserId = function(req,res){
  var userId = req.params.Id;
  repository
  .GetFavorsByUserId(userId)
  .then(function(models){
    res.status(200).send(models);
  })
  .catch(function(err){
    res.status(500).send(err);
  })

}

controller.GetFavorsAll = function(req,res){
  repository
  .GetFavorsAll()
  .then(function(models){
    res.status(200).send(models);
  })
  .catch(function(err){
    res.status(500).send(err);
  })
}

controller.GetFavorsAllActive = function(req,res){
  repository
  .GetFavorsAllActive()
  .then(function(models){
    res.status(200).send(models);
  })
  .catch(function(err){
    res.status(500).send(err);
  })
}

module.exports = controller;
