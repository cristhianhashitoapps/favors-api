var models = require('../../models');
var repository = require('../../repositories/Fvposibletaker');
var controller = require('./BaseController')(repository);
var repositoryFavor = require('../../repositories/Fvfavor');
var repositoryUser = require('../../repositories/Fvuser');

//inicio notificaciones
var admin = require("firebase-admin");
var serviceAccount = require("../../salvadia-fedb5-firebase-adminsdk-79h8x-0a3feb7b50.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});
//fin notificaciones

controller.GetPosibleTakersByFavor = function (req, res) {
  var favorId = req.params.Id;
  repository
    .GetPosibleTakersByFavor(favorId)
    .then(function (models) {
      res.status(200).send(models);
    })
    .catch(function (err) {
      res.status(500).send(err);
    })
}

controller.GetAverageCalificationsByTakerId = function (req, res) {
  var takerId = req.params.Id;
  repository
    .GetAverageCalificationsByTakerId(takerId)
    .then(function (models) {
      //
      var response = {};

      var favors = models.rows;
      var averageCalifications = 0, sumCalifications = 0, sumOpinion = 0;
      favors.
        forEach(function (element) {
          sumCalifications = sumCalifications + element.Fvfavor.FvtypeofcalificationId;
          if (element.Fvfavor.opinion !== null) {
            sumOpinion = sumOpinion + 1;
          }
        });
      averageCalifications = sumCalifications / models.count;

      response.countFavors = models.count;
      response.averageCalifications = averageCalifications;
      response.sumOpinions = sumOpinion;

      res.status(200).send(response);

    })
    .catch(function (err) {
      res.status(500).send(err);
    })
}

controller.GetOpinionsByTakerId = function (req, res) {
  var takerId = req.params.Id;
  repository
    .GetOpinionsByTakerId(takerId)
    .then(function (models) {
      //
      var response = [];
      var favors = models.rows;

      favors.
        forEach(function (element) {

          if (element.Fvfavor.opinion !== null) {
            var opinion = { "opinion": element.Fvfavor.opinion };
            response.push(opinion);
          }
        });

      res.status(200).send(response);

    })
    .catch(function (err) {
      res.status(500).send(err);
    })
}

controller.GetPosibleTakersBySalvadiaId = function (req, res) {
  var salvadiaId = req.params.Id;
  var response = [];


  repository
    .GetPosibleTakersBySalvadiaId(salvadiaId)
    .then(function (models) {
      //
      var response = [];
      var favors = models.rows;
      favors
        .forEach(function (element) {
          //if (element.FvposibletakerstateId > 1) {
            response.push(element.Fvfavor);
          //}
        })

      var result = {
        rows: response
      }


      res.status(200).send(result);

    })
    .catch(function (err) {
      res.status(500).send(err);
    })
}

controller.UpdatePosibleTaker = function (req, res) {

  console.log("actualiza");
  console.log(req.body);
  repository
    .UpdatePosibleTaker(req.params.Id, req.body)
    .then(function (resx) {      
      sendNotification(req.body);
      res.status(200).send(resx);
    })
    .catch(function (err) {
      console.log(err);
      res.status(500).send(err);
    })
}

function sendNotification(data) {

  repositoryFavor
    .LoadById(data.FvfavorId)
    .then(function(resFavor){
        var des = resFavor.description;
        //consultamos el token del usuario posible taker
        repositoryUser
          .LoadById(data.FvuserId)
          .then(function(resUser){
              data.post={
                action: "notifyPosibleTaker",
                message: "Salvadia - Usted ha sido asignado al Favor: "+des
              }

              var registrationToken = [resUser.tokenfcm];
              var payload = {
                notification: {
                  "title": "Salvadia",
                  "body": JSON.stringify(data.post),
                  "notId": "10"
                },

              data: {
                  mensaje: JSON.stringify(data.post)
                }
              };

              admin.messaging().sendToDevice(registrationToken, payload)
                .then(function (response) {
                  console.log("Successfully sent message:", response);
                })
                .catch(function (error) {
                  console.log("Error sending message:", error);
                });

            })
            .catch(function(err){
              console.log(err);
            })
    })
    .catch(function(err){
      console.log(err);
    })  
}

module.exports = controller;
