var models     = require('../../models');
var repository = require('../../repositories/Fvtransaction');
var controller = require('./BaseController')(repository);
var request = require('request');
var md5 = require('md5');

controller.SendToPayu = function(req,res,next){
  var md5 = md5ForString("cristhian");
  var postObject = getDataPayu();
  request(postObject, function(error,response,body){
    //console.log(error);
    //console.log(response);
    console.log(body);
  });
  res.status(200).send("ok");
}

var md5ForString = function(param){
  console.log(md5(param));
}

controller.GetCompleteTransaction = function(req,res){
  repository
    .GetCompleteTransaction(req.params.Id)
    .then(function(model){
      res.status(200).send(model);
    })
    .catch(function(error){
      console.log(error);
      res.status(500).send(error);
    })
}

controller.GetCompleteApproved = function(req,res){
  repository
    .GetCompleteApproved()
    .then(function(model){
      res.status(200).send(model);
    })
    .catch(function(error){
      console.log(error);
      res.status(500).send(error);
    })
}

var getDataPayu = function(){
  /*
  var accountId = "512321";
  var apiLogin = "pRRXKOl8ikMmt9u";
  var apiKey = "4Vj8eK4rloUd272L48hsrarnUA";
  var merchantId = "508029";
  */
  var accountId = "761250";
  var apiLogin = "eMojSDy0vD2iL0i";
  var apiKey = "4eZL9R35am3JkbQ7Q2x4L6b8w6";
  var merchantId = "755374";

  var timestamp = new Date().getUTCMilliseconds();
  var referenceCode = ["testPanama1",timestamp].join("");
  var total = "50000";
  var currency = "COP";
  var signatureJoin = [apiKey,merchantId,referenceCode,total,currency].join("~");
  var signature = md5(signatureJoin);

  var number= "4103450631456519";
  var securityCode = "658";
  var expirationDate ="2025/09";
  var name = "test";

  var res = {
     "language": "es",
     "command": "SUBMIT_TRANSACTION",
     "merchant": {
        "apiLogin": apiLogin,
        "apiKey": apiKey
     },
     "transaction": {
        "order": {
           "accountId": accountId,
           "referenceCode": referenceCode,
           "description": "Test order Panama",
           "language": "en",
           "notifyUrl": "http://pruebaslap.xtrweb.com/lap/pruebconf.php",
           "signature": signature,
           "shippingAddress": {
              "country": "CO"
           },
           "buyer": {
              "fullName": "APPROVED",
              "emailAddress": "test@payulatam.com",
              "dniNumber": "1155255887",
              "shippingAddress": {
                 "street1": "Calle 93 B 17 – 25",
                 "city": "Bogota",
                 "state": "Colombia",
                 "country": "CO",
                 "postalCode": "000000",
                 "phone": "5582254"
              }
           },
           "additionalValues": {
              "TX_VALUE": {
                 "value": total,
                 "currency": currency
              }
           }
        },
        "creditCard": {
           "number": number,
           "securityCode": securityCode,
           "expirationDate": expirationDate,
           "name": name
        },
        "type": "AUTHORIZATION_AND_CAPTURE",
        "paymentMethod": "VISA",
        "paymentCountry": "CO",
        "payer": {
           "fullName": "APPROVED",
           "emailAddress": "test@payulatam.com"
        },
        "ipAddress": "127.0.0.1",
        "cookie": "cookie_52278879710130",
        "userAgent": "Firefox",
        "extraParameters": {
           "INSTALLMENTS_NUMBER": 1,
           "RESPONSE_URL": "http://www.misitioweb.com/respuesta.php"
        }
     },
     "test": false
  }

  var   options = {
        //uri: 'https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi',
        uri: 'https://api.payulatam.com/payments-api/4.0/service.cgi',
        body: JSON.stringify(res),
        method: 'POST',
        headers: {
             'Content-Type': 'application/json',
             'Accept':"application/json"
       }
     }

     console.log(signatureJoin);
     return options;
}

controller.ConfirmPayu = function(req,res){
   res.status(200).send("ok");
}

controller.GetTransactionsByUserId = function(req,res){

   var result=[];

   repository
     .GetCompleteApproved()
     .then(function(model){

         model
         .rows
            .forEach(function(ele){                 
                if(ele.Fvfavor.FvuserId==req.params.Id) {                   
                   result.push(ele);
                } 
            })

       res.status(200).send(result);
     })
     .catch(function(error){
       console.log(error);
       res.status(500).send(error);
     })
 }

 controller.GetTransactionsBySalvadiaId = function(req,res){

   var result=[];

   repository
     .GetCompleteApproved()
     .then(function(model){

         model
         .rows
            .forEach(function(ele){                 
                if(ele.Fvfavor.FvuserId==req.params.Id) {                   
                   result.push(ele);
                } 
            })

       res.status(200).send(result);
     })
     .catch(function(error){
       console.log(error);
       res.status(500).send(error);
     })
 }


 controller.GetByFavorId = function(req,res){

   repository
     .GetByFavorId(req.params.Id)
     .then(function(model){       

       res.status(200).send(model);
     })
     .catch(function(error){
       console.log(error);
       res.status(500).send(error);
     })
 }
module.exports = controller;
