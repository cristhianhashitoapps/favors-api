var models = require('../../models');
var repository = require('../../repositories/Fvuser');
var controller = require('./BaseController')(repository);
var fs = require('fs');
var multer = require('multer');
var path = require('path');
var sequelize = require('sequelize');
var bcrypt = sequelize.Promise.promisifyAll(require('bcryptjs'));
var SALT_WORK_FACTOR = 10;

var nodemailer    = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth: {
        user: 'hashitoapps@gmail.com', // my mail
        pass: 'hashito2016'
    }
}));

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  filename: function (req, file, callback) {
    console.log("..................................");
    console.log(file);
    console.log("..................................");
    callback(null, file.originalname);
  }
});
var upload = multer({ storage: storage }).single('userPhoto');

//new
controller.HashPassword = function(user){
  return bcrypt
      .genSaltAsync(SALT_WORK_FACTOR)
      .then(function (salt) {
        console.log("eeee");
        return bcrypt
          .hashAsync(user.password, salt)
      })
      .then(function (hash) {
        user.password = hash;

        return sequelize.Promise.resolve(user);
      });

}

controller.Login = function (req, res) {
  models
    .Fvuser
    .Login(req.body.login, req.body.password)
    .then(function (result) {
      res.send({
        Token: result
      });
    })
    .catch(function (error) {
      res.status(500).send(error);
    });
};

controller.GetImage = function (req, res) {
  var params = req.params;

  repository
    .LoadById(params.Id)
    .then(function (model) {
      if (model === null || model === undefined) {
        res.status(404).send('No existe un objeto con ese Id');
      } else {
        console.log(model.img);
        console.log(model.img !== "");
        if (model.img !== "") {
          var img = fs.readFileSync('./uploads/' + model.img);
          res.writeHead(200, { 'Content-Type': 'image/gif' });
          res.end(img, 'binary');
        } else {
          res.writeHead(200, { 'Content-Type': 'image/gif' });
          res.end("", 'binary');
        }
      }
    })
    .catch(function (error) {
      res.status(500).send(error);
    });

};
controller.GetStatistics = function (req, res) {
  console.log("GetStatistics");
  var params = req.params;
  console.log("GetStatistics", params.Id);
  repository
    .getStatistics(params.Id)
    .then(function (model) {
      if (model === null || model === undefined) {
        res.status(404).send('No existe un objeto con ese Id');
      } else {
        console.log("----------------------------------------");
        console.log("----------------------------------------");
        console.log("----------------------------------------");
        console.log(model);
        console.log("----------------------------------------");
        console.log("----------------------------------------");
        console.log("----------------------------------------");
        res.send(model);
      }
    })
    .catch(function (error) {
      res.status(500).send(error);
    });
};

controller.Image = function (req, res) {
  upload(req, res, function (err) {
    if (err) {
      return res.end("Error uploading file.");
    }
    console.log(req.options);
    console.log(req.body.user);
    console.log("-----UPLOADING IMAGE---------", res.req.file.filename);
    console.log(res.req.file.filename);
    console.log("-----UPLOADING IMAGE---------");
    res.status(200).send({ id: res.req.file.filename });
  });
};

controller.Signup = function (req, res) {
  let user = req.body;
  let originalPass = req.body.password;

  controller.HashPassword(user).then(function(user_){
    repository
    .Insert(user_)
    .then(function (result) {
      console.log("inserta");
      //enviar correo a salvadia
      if(req.body.typeofuser==true){
        controller.enviarCorreo(req.body);
      }

      console.log("---------------------");  
      console.log(req.body.login);
      console.log(originalPass);
      console.log("---------------------");  

      return models
        .Fvuser
        .Login(req.body.login, originalPass);
    })
    .then(function (result) {
      res.send({
        Token: result
      });
    })
    .catch(function (error) {
      console.log(error);
      res.status(500).send(error);
    });
  })
  .catch(function(error){
    console.log(error);
    res.status(500).send(error);
  })
  
  
  
};

controller.enviarCorreo = function(user){
    var mailOptions = {
      from: 'Salvadia-Backend <salvadiabackend@hotmail.com>',
      to: 'salvadiacol@gmail.com,oscarb@wasabi.com.co',
      subject: ['Nuevo Salvadia:', user.login].join(' ')
    };

    //mailOptions.text = body.Message;
    mailOptions.html = "<h1><i> Mensaje desde: Salvadia-Backend</i></h1><br>";
    mailOptions.html += ["Login",user.login,'<br>'].join(' ');
    mailOptions.html += ["Nombre",user.firstname,user.lastname,'<br>'].join(' ');
    mailOptions.html += ["Correo",user.email,'<br>'].join(' ');


    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Message sent: ' + info.response);
      }
    });
}

function generateUUID() {
  var d = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
};

controller.RecoverPassword = function(req,res){
  var email = req.params.Id;

  repository
    .getByEmail(email)
    .then(function(result){      
      var str = "" + result.id;
      var pad = "0000000000"
      var ans = pad.substring(0, pad.length - str.length) + str
      var finalCad = ans+generateUUID();
      
      //send email for recover password
      controller.sendMailToRecoverPassword(finalCad,result.email,result.login);
      var exito = {
        "finalCad":finalCad
      }
      res.status(200).send(exito);
    })
    .catch(function(error){
      console.log(error);
      res.status(500).send(error);
    })

  
}

controller.sendMailToRecoverPassword = function(finalCad,emailUser,login){
  var mailOptions = {
    from: 'Salvadia-Backend <salvadiabackend@hotmail.com>',
    to: ['salvadiacol@gmail.com',emailUser].join(','),
    subject: ['Recuperar contraseña Salvadia',finalCad].join(' ')
  };

  //mailOptions.text = body.Message;
  mailOptions.html = "<h1><i> Mensaje desde: Salvadia</i></h1><br>";
  mailOptions.html +=  ["<h2>Su usuario es: ",login,"</h2><br>"].join('');
  mailOptions.html += ["Da click en este enlace o pega en un navegador para recuperar la contraseña:",'<br>'].join(' ');
  mailOptions.html += ["http://backend.salvadia.com/new-password/",finalCad].join('');

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Message sent: ' + info.response);
    }
  });
}

controller.UpdateUser = function(req,res){
  var user = req.body;
  console.log(user);
  controller.HashPassword(user).then(function(user_){

    repository
      .Update(user_.id,user_)
      .then(function(response){
        res.status(200).send(response);
      })
      .catch(function(error){
        console.log(error)
        res.status(500).send(error);
      })

    })
    .catch(function(err){
      res.status(500).send(err);
    })
}


module.exports = controller;
