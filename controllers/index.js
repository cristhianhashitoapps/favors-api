var controllers = [];

controllers['v00a'] = {
  Fvuser: require('./v00a/Fvuser'),
  Fvbank: require('./v00a/Fvbank'),
  Fvcreditcard: require('./v00a/Fvcreditcard'),
  Fvfavor: require('./v00a/Fvfavor'),
  Fvfavorandtype: require('./v00a/Fvfavorandtype'),
  Fvfavorstate: require('./v00a/Fvfavorstate'),
  Fvfavortype: require('./v00a/Fvfavortype'),
  Fvposibletaker: require('./v00a/Fvposibletaker'),
  Fvtransaction: require('./v00a/Fvtransaction'),
  Fvtransactionstate: require('./v00a/Fvtransactionstate'),
  Fvtypeofcalification: require('./v00a/Fvtypeofcalification'),
  Fvposibletakerstate: require('./v00a/Fvposibletakerstate'),
  Fvaddressfield: require('./v00a/Fvaddressfield'),
  Fvcity: require('./v00a/Fvcity'),
  Fvstate: require('./v00a/Fvstate'),
  Fvcountry: require('./v00a/Fvcountry'),
  Fvaddress: require('./v00a/Fvaddress'),
  Fvcupon: require('./v00a/Fvcupon'),
  Fvcuponuser: require('./v00a/Fvcuponuser'),
};

module.exports = controllers;
