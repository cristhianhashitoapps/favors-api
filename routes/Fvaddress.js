module.exports = function (apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express = require('express');
  var router = express.Router();
  var multipart = require('connect-multiparty')();
  var auth = require('./auth');


  router.get('/fvaddresses', auth,controllers.Fvaddress.GetAll);
  router.get('/fvaddresses/:Id', auth,controllers.Fvaddress.byId);
  router.post('/fvaddresses', auth,controllers.Fvaddress.Insert);
  router.put('/fvaddresses/:Id', auth,controllers.Fvaddress.Update);
  router.delete('/fvaddresses/:Id', auth,controllers.Fvaddress.Delete);
  return router;
};
