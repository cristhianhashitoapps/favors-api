module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvcity',auth,controllers.Fvcity.GetAll);
  router.get('/fvcity/:Id',auth,controllers.Fvcity.GetById);
  router.post('/fvcity',auth,controllers.Fvcity.Insert);
  router.put('/fvcity/:Id',auth,controllers.Fvcity.Update);
  router.delete('/fvcity/:Id',auth,controllers.Fvcity.Delete);
  return router;
};
