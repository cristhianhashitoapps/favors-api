module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvfavor',auth,controllers.Fvfavor.GetAll);
  router.get('/fvfavor/:Id',auth,controllers.Fvfavor.GetById);
  router.post('/fvfavor',auth,controllers.Fvfavor.Insert);
  router.put('/fvfavor/:Id',auth,controllers.Fvfavor.Update);
  router.delete('/fvfavor/:Id',auth,controllers.Fvfavor.Delete);
  router.get('/fvfavor/:Id/getfavorsbyuserid',auth,controllers.Fvfavor.GetFavorsByUserId);
  router.post('/fvfavor/all/',auth,controllers.Fvfavor.GetFavorsAll);
  router.post('/fvfavor/allactive/',auth,controllers.Fvfavor.GetFavorsAllActive);
  return router;
};
