module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvtypeofcalification',auth,controllers.Fvtypeofcalification.GetAll);
  router.get('/fvtypeofcalification/:Id',auth,controllers.Fvtypeofcalification.GetById);
  router.post('/fvtypeofcalification',auth,controllers.Fvtypeofcalification.Insert);
  router.put('/fvtypeofcalification/:Id',auth,controllers.Fvtypeofcalification.Update);
  router.delete('/fvtypeofcalification/:Id',auth,controllers.Fvtypeofcalification.Delete);
  return router;
};
