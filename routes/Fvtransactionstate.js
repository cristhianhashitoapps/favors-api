module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvtransactionstate',auth,controllers.Fvtransactionstate.GetAll);
  router.get('/fvtransactionstate/:Id',auth,controllers.Fvtransactionstate.GetById);
  router.post('/fvtransactionstate',auth,controllers.Fvtransactionstate.Insert);
  router.put('/fvtransactionstate/:Id',auth,controllers.Fvtransactionstate.Update);
  router.delete('/fvtransactionstate/:Id',auth,controllers.Fvtransactionstate.Delete);
  return router;
};
