module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvfavortype',auth,controllers.Fvfavortype.GetAll);
  router.get('/fvfavortype/:Id',auth,controllers.Fvfavortype.GetById);
  router.post('/fvfavortype',auth,controllers.Fvfavortype.Insert);
  router.put('/fvfavortype/:Id',auth,controllers.Fvfavortype.Update);
  router.delete('/fvfavortype/:Id',auth,controllers.Fvfavortype.Delete);
  return router;
};
