module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.post('/fvcuponuser/getbycuponuser',auth,controllers.Fvcuponuser.GetByUserAndCupon);
  router.post('/fvcuponuser/',auth,controllers.Fvcuponuser.Insert);
  
  return router;
};
