module.exports = function (apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express = require('express');
  var router = express.Router();
  var multipart = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvuser',auth, controllers.Fvuser.GetAll);
  router.get('/fvuser/:Id',auth, controllers.Fvuser.GetById);
  router.get('/fvuser/:Id/statistics',auth, controllers.Fvuser.GetStatistics);
  router.post('/fvuser', auth,controllers.Fvuser.Insert);
  router.post('/fvuser/login', auth,controllers.Fvuser.Login);
  router.post('/fvuser/signup', auth,controllers.Fvuser.Signup);
  router.post('/fvusers', controllers.Fvuser.Image);
  router.get('/fvusers/:Id/img', controllers.Fvuser.GetImage);
  router.put('/fvuser/:Id', auth,controllers.Fvuser.Update);
  router.delete('/fvuser/:Id', auth,controllers.Fvuser.Delete);
  router.get('/fvuser/:Id/recoverpass',auth,controllers.Fvuser.RecoverPassword);
  router.put('/fvuser/:Id/update', auth,controllers.Fvuser.UpdateUser);
  return router;
};
