module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
    var auth = require('./auth');

  router.get('/fvposibletakerstate',auth,controllers.Fvposibletakerstate.GetAll);
  router.get('/fvposibletakerstate/:Id',auth,controllers.Fvposibletakerstate.GetById);
  router.post('/fvposibletakerstate',auth,controllers.Fvposibletakerstate.Insert);
  router.put('/fvposibletakerstate/:Id',auth,controllers.Fvposibletakerstate.Update);
  router.delete('/fvposibletakerstate/:Id',auth,controllers.Fvposibletakerstate.Delete);
  return router;
};
