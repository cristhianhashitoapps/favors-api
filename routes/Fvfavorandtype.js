module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvfavorandtype',auth,controllers.Fvfavorandtype.GetAll);
  router.get('/fvfavorandtype/:Id',auth,controllers.Fvfavorandtype.GetById);
  router.post('/fvfavorandtype',auth,controllers.Fvfavorandtype.Insert);
  router.put('/fvfavorandtype/:Id',auth,controllers.Fvfavorandtype.Update);
  router.delete('/fvfavorandtype/:Id',auth,controllers.Fvfavorandtype.Delete);
  return router;
};
