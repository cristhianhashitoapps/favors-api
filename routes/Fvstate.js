module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvstate',auth,controllers.Fvstate.GetAll);
  router.get('/fvstate/:Id',auth,controllers.Fvstate.GetById);
  router.post('/fvstate',auth,controllers.Fvstate.Insert);
  router.put('/fvstate/:Id',auth,controllers.Fvstate.Update);
  router.delete('/fvstate/:Id',auth,controllers.Fvstate.Delete);
  return router;
};
