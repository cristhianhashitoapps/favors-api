module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');


  router.get('/fvbank',auth,controllers.Fvbank.GetAll);
  router.get('/fvbank/:Id',auth,controllers.Fvbank.GetById);
  router.post('/fvbank',auth,controllers.Fvbank.Insert);
  router.put('/fvbank/:Id',auth,controllers.Fvbank.Update);
  router.delete('/fvbank/:Id',auth,controllers.Fvbank.Delete);
  return router;
};
