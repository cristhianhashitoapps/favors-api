module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvcupon',auth,controllers.Fvcupon.GetAll);
  router.get('/fvcupon/:Id/getbyname',auth,controllers.Fvcupon.GetByName);
  return router;
};
