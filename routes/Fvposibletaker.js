module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvposibletaker',auth,controllers.Fvposibletaker.GetAll);
  router.get('/fvposibletaker/:Id',auth,controllers.Fvposibletaker.GetById);
  router.post('/fvposibletaker',auth,controllers.Fvposibletaker.Insert);
  router.put('/fvposibletaker/:Id',auth,controllers.Fvposibletaker.Update);
  router.delete('/fvposibletaker/:Id',auth,controllers.Fvposibletaker.Delete);
  router.get('/fvposibletaker/:Id/getposibletakersbyfavor',auth,controllers.Fvposibletaker.GetPosibleTakersByFavor);
  router.get('/fvposibletaker/:Id/getaveragecalificationsbytakerid',auth,controllers.Fvposibletaker.GetAverageCalificationsByTakerId);
  router.get('/fvposibletaker/:Id/getopinionsbytakerid',auth,controllers.Fvposibletaker.GetOpinionsByTakerId);
  router.get('/fvposibletaker/:Id/getposibletakerbysalvadiaid',auth,controllers.Fvposibletaker.GetPosibleTakersBySalvadiaId);
  router.put('/fvposibletaker/:Id/update',auth,controllers.Fvposibletaker.UpdatePosibleTaker);
  return router;
};
