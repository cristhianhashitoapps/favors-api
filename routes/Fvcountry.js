module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvcountry',auth,controllers.Fvcountry.GetAll);
  router.get('/fvcountry/:Id',auth,controllers.Fvcountry.GetById);
  router.post('/fvcountry',auth,controllers.Fvcountry.Insert);
  router.put('/fvcountry/:Id',auth,controllers.Fvcountry.Update);
  router.delete('/fvcountry/:Id',auth,controllers.Fvcountry.Delete);
  return router;
};
