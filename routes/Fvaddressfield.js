module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvaddressfield',auth,controllers.Fvaddressfield.GetAll);
  router.get('/fvaddressfield/:Id',auth,controllers.Fvaddressfield.GetById);
  router.post('/fvaddressfield',auth,controllers.Fvaddressfield.Insert);
  router.put('/fvaddressfield/:Id',auth,controllers.Fvaddressfield.Update);
  router.delete('/fvaddressfield/:Id',auth,controllers.Fvaddressfield.Delete);
  return router;
};
