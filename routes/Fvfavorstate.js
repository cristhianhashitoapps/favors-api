module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvfavorstate',auth,controllers.Fvfavorstate.GetAll);
  router.get('/fvfavorstate/:Id',auth,controllers.Fvfavorstate.GetById);
  router.post('/fvfavorstate',auth,controllers.Fvfavorstate.Insert);
  router.put('/fvfavorstate/:Id',auth,controllers.Fvfavorstate.Update);
  router.delete('/fvfavorstate/:Id',auth,controllers.Fvfavorstate.Delete);
  return router;
};
