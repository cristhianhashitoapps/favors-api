module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  router.get('/fvcreditcard',auth,controllers.Fvcreditcard.GetAll);
  router.get('/fvcreditcard/:Id',auth,controllers.Fvcreditcard.GetById);
  router.post('/fvcreditcard',auth,controllers.Fvcreditcard.Insert);
  router.put('/fvcreditcard/:Id',auth,controllers.Fvcreditcard.Update);
  router.delete('/fvcreditcard/:Id',auth,controllers.Fvcreditcard.Delete);
  router.get('/fvcreditcard/:Id/getbyuser',auth,controllers.Fvcreditcard.GetCreditCardsByUser);
  router.post('/fvcreditcard/savecreditcardinpayu',auth,controllers.Fvcreditcard.SaveCreditCardInPayu);
  router.post('/fvcreditcard/deletecreditcardinpayu',auth,controllers.Fvcreditcard.DeleteCreditCardInPayu);
  router.post('/fvcreditcard/paywithpayu',auth,controllers.Fvcreditcard.PayWithPayu);
  return router;
};
