module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();
  var auth = require('./auth');

  //consulta las transacciones aprobadas por usuario_id
  router.get('/fvtransaction/',auth,controllers.Fvtransaction.GetCompleteApproved);
  router.get('/fvtransaction/:Id',auth,controllers.Fvtransaction.GetById);
  router.post('/fvtransaction',auth,controllers.Fvtransaction.Insert);
  router.put('/fvtransaction/:Id',auth,controllers.Fvtransaction.Update);
  router.delete('/fvtransaction/:Id',auth,controllers.Fvtransaction.Delete);
  router.post('/fvtransaction/sendToPayu/',auth,controllers.Fvtransaction.SendToPayu);
  router.get('/fvtransaction/:Id/getcomplete',auth,controllers.Fvtransaction.GetCompleteTransaction);
  router.post('/fvtransaction/confirmpayu',controllers.Fvtransaction.ConfirmPayu);
  router.get('/fvtransaction/:Id/getbyuserid',controllers.Fvtransaction.GetTransactionsByUserId);
  router.get('/fvtransaction/:Id/getbyfavorid',controllers.Fvtransaction.GetByFavorId);

  return router;
};
