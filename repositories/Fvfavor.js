var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Fvfavor);

var includes   = [{
    model: models.Fvuser
  },
  {
    model: models.Fvfavorstate
  },
  {
    model: models.Fvtypeofcalification
  }
];

repository.Validate = function(model) {
  var error = null;
  if (model.description === undefined || model.description === null) {
    error = 'Invalid description';
  }
  if (model.durationhours === undefined || model.durationhours === null) {
    error = 'Invalid durationhours';
  }
  if (model.price === undefined || model.price === null) {
    error = 'Invalid price';
  }
  return error;
}

repository.GetFavorsByUserId = function(userId) {

  return new Promise(function(resolve, reject) {
    models.Fvfavor
      .findAndCount({
        where: {
          FvuserId: userId          
        },        
        include: includes,
        order:[
          ['createdAt','DESC']
        ]
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}

repository.GetFavorsAll = function(){
  return new Promise(function(resolve, reject) {
    models.Fvfavor
      .findAndCount({       
        include: includes,        
        order: [
          ['createdAt','DESC']          
        ]
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}

repository.GetFavorsAllActive = function(){
  return new Promise(function(resolve, reject) {
    models.Fvfavor
      .findAndCount({       
        include: includes,
        where: {
          active : '1' 
        } ,
        order: [
          ['createdAt','DESC']          
        ]
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}



module.exports = repository;
