var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Fvbank);

repository.Validate = function(model) {
  var error = null;
  if (model.name === undefined || model.name === null) {
    error = 'Invalid name';
  }
  return error;
}

module.exports = repository;
