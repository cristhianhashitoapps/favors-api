var models = require('../models');
var Promise = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Fvaddress);

repository.Validate = function (model) {
  var error = null;
  if (model.name === undefined || model.name === null) {
    error = 'Invalid name';
  }
  return error;
}

repository.byId = function (id) {
  console.log("Repository", "byId", id);
  return models.Fvaddress
    .find({
      where: {
        id: id
      },
      include: [{
        model: models.Fvaddressfield
      },
      {
        model: models.Fvcity
      }]
    });
}

module.exports = repository;
