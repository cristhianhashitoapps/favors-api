var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Fvcreditcard);

var includes   = [{
    model: models.Fvuser
  },
  {
    model: models.Fvbank
  }
];

repository.Validate = function(model) {
  var error = null;
  if (model.number === undefined || model.number === null) {
    error = 'Invalid name';
  }
  if (model.securitycode === undefined || model.securitycode === null) {
    error = 'Invalid securitycode';
  }
  if (model.finaldate === undefined || model.finaldate === null) {
    error = 'Invalid finaldate';
  }
  return error;
}

repository.GetCreditCardsByUser =function(userId){
  return new Promise(function(resolve, reject) {
    models.Fvcreditcard
      .findAndCount({
        where: {
          FvuserId: userId,
          active: '1'
        },
        include: includes
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}



module.exports = repository;
