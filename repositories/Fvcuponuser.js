var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Fvcuponuser);

repository.Validate = function(model) {
  var error = null;
  /*if (model.name === undefined || model.name === null) {
    error = 'Invalid name';
  }*/
  return error;
}

repository.GetByUserAndCupon = function(cupon,FvuserId){
  return new Promise(function(resolve, reject) {
    models.Fvcuponuser
      .findAndCount({
        where: {
          cupon: cupon,
          FvuserId: FvuserId
        }    
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });

}

module.exports = repository;
