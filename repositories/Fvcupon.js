var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Fvcupon);

repository.Validate = function(model) {
  var error = null;
  if (model.name === undefined || model.name === null) {
    error = 'Invalid name';
  }
  return error;
}

repository.GetByName = function(name){
  return new Promise(function(resolve, reject) {
    models.Fvcupon
      .findAndCount({
        where: {
          name: name,
          active: '1'
        }    
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });

}

module.exports = repository;
