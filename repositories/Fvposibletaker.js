var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Fvposibletaker);

var includes   = [{
    model: models.Fvuser
  },
  {
    model: models.Fvposibletakerstate
  },
  {
    model: models.Fvfavor
  },
];

repository.Validate = function(model) {
  var error = null;
  if (model.efective === undefined || model.efective === null) {
    error = 'Invalid efective';
  }
  return error;
}

repository.GetPosibleTakersByFavor = function(favorId){
  return new Promise(function(resolve, reject) {
    models.Fvposibletaker
      .findAndCount({
        where: {
          FvfavorId: favorId
        },
        include: includes
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}

repository.GetAverageCalificationsByTakerId = function(takerId){

  return new Promise(function(resolve, reject) {
    models.Fvposibletaker
      .findAndCount({
        where: {
          FvuserId: takerId,
          efective: true
        },
        include: includes
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}

repository.GetOpinionsByTakerId = function(takerId){

  return new Promise(function(resolve, reject) {
    models.Fvposibletaker
      .findAndCount({
        where: {
          FvuserId: takerId,
          efective: true
        },
        include: includes
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}

repository.GetPosibleTakersBySalvadiaId = function(salvadiaId){
  return new Promise(function(resolve, reject) {
    models.Fvposibletaker
      .findAndCount({
        where: {
          FvuserId: salvadiaId,
          efective: true
        },
        include: includes,
        order: [
          ['id','DESC']
        ]
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}

repository.UpdatePosibleTaker = function(Id, newModel) {  
  return new Promise(function(resolve, reject) {
    repository
      .LoadById(Id)
      .then(function(model) {
        if (model === null) {
          return reject('Can\'t find object that Id');
        }

        return resolve(model.updateAttributes(newModel));
      })      
  });
};


module.exports = repository;
