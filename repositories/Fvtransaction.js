var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Fvtransaction);

var includes   = [{
    model: models.Fvfavor,
    include: models.Fvuser
  },
  {
    model: models.Fvcreditcard,
    include: models.Fvbank
  },
  {
    model: models.Fvtransactionstate
  }
];

repository.Validate = function(model) {
  var error = null;
  return error;
}

repository.GetCompleteTransaction = function(id){
  console.log("completeeeee");
  return new Promise(function(resolve, reject) {
    models.Fvtransaction
      .findAndCount({
        where: {
          id: id
        },
        include: includes
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}

repository.GetCompleteApproved = function(){
  console.log("se ejecuta approved");
  return new Promise(function(resolve, reject) {
    models.Fvtransaction
      .findAndCount({
        where: {
          payustate:'APPROVED'
        },
        include:
        [{
            model: models.Fvfavor,
            include: models.Fvuser
          },
          {
            model: models.Fvcreditcard,
            include: models.Fvbank
          },
          {
            model: models.Fvtransactionstate
          }
        ]
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}

repository.GetByFavorId = function(favorId){
  return new Promise(function(resolve, reject) {
    models.Fvtransaction
      .findAndCount({
        where: {
          FvFavorId:favorId
        },
        include:
        [{
            model: models.Fvfavor,
            include: models.Fvuser
          },
          {
            model: models.Fvcreditcard,
            include: models.Fvbank
          },
          {
            model: models.Fvtransactionstate
          }
        ]
      })
      .then(function(model) {
        return resolve(model);
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}


module.exports = repository;
