var models = require('../models');
var Promise = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Fvuser);

repository.Validate = function (model) {
  var error = null;
  if (model.login === undefined || model.login === null) {
    error = 'Invalid login';
  }
  if (model.password === undefined || model.password === null) {
    error = 'Invalid password';
  }
  if (model.email === undefined || model.email === null) {
    error = 'Invalid email';
  }
  if (model.firstname === undefined || model.firstname === null) {
    error = 'Invalid firstname';
  }
  if (model.lastname === undefined || model.lastname === null) {
    error = 'Invalid lastname';
  }
  return error;
}

repository.getStatistics = function (id) {
  console.log("FvuserRepository", "getStatistics", id);
  return models.Fvfavor
    .findAndCount({
      where: {
        FvuserId: id
      },
      include: this.includes
    });
}

repository.getByEmail = function(_email){
  return models.Fvuser
    .find({
      where: {
        email: _email
      }
    });
}

module.exports = repository;
