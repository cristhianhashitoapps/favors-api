"use strict";

module.exports = {
  up: function(queryInterface, Sequelize, DataTypes) {
    return queryInterface.createTable('Fvcuponusers', {
      id: { type: Sequelize.INTEGER, primaryKey : true, autoIncrement: true},
      FvuserId: { type: Sequelize.INTEGER, allowNull: false},
      cupon: { type: Sequelize.STRING, allowNull: false},      
      createdAt: { type: Sequelize.DATE, allowNull: false},
      updatedAt: { type: Sequelize.DATE, allowNull: false},
          });
  },

  down: function(queryInterface, Sequelize) {
    // add reverting commands here, calling 'done' when finished
    done();
  }
};
