"use strict";

module.exports = { 

  up: (queryInterface, Sequelize, DataTypes) => {    

    return queryInterface.addColumn(
      'Fvtransactions', // name of Source model
      'cupon', // name of the key we're adding 
        {
          type: Sequelize.STRING,                
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          allowNull: true,
        }
      );      
},

  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
    done();
  }
};
