'use strict';

module.exports = {
  up: (queryInterface, Sequelize, DataTypes) => {    

      return queryInterface.createTable('Fvcupons', {
        id: { type: Sequelize.INTEGER, primaryKey : true, autoIncrement: true},
        name: { type: Sequelize.STRING, allowNull: false},
        date: { type: Sequelize.DATE, allowNull: false},
        active: { type: Sequelize.BOOLEAN, allowNull: false, default: true},
        createdAt: { type: Sequelize.DATE, allowNull: false},
        updatedAt: { type: Sequelize.DATE, allowNull: false},
            });

        
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
